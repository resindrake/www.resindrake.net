module.exports = {
	createSchema: (pages, config) => {
		return `
type Query {
	pageByIndex(index: Int!): Page
	pageByTitle(title: String!): Page
	pages: [Page]
	linkedPages: [Page]
	projectPages: [Page]
	posts: [Page]
}
type Page {
	html: String
	frontmatter: Frontmatter
	excerpt: String
	relativePath: String
	active: Boolean
	headerlink: Boolean
}
type Frontmatter {
	title: String
	description: String
	date: String
	layout: String
	rainbow: Boolean
	gallery: [Image]
	redirect: String
}
type Image {
	path: String
	link: String
	display: String
	next: Image
	prev: Image
}
		`;
	},
	createRoot: (pages, config) => {
		return {
			pages: () => pages.filter(p => p.frontmatter.headerlink === true),
			pageByIndex: args => pages[args.index],
			pageByTitle: args => pages.filter(p => p.frontmatter.tile === args.title),
			projectPages: () => pages.filter(p => p.relativePath.startsWith("/projects/") && p.relativePath !== "/projects/index.html"),
			posts: args => pages.filter(p => p.frontmatter.type === "blog")
		};
	}
};
