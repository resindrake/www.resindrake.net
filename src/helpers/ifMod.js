module.exports = (arg1, arg2, options) => ((arg1 % arg2 == 0) ? options.fn(this) : options.inverse(this));
