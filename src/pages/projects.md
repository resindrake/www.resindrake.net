---
layout: projects
title: Projects
description: List of my projects
query: '{
	pages {
		frontmatter {
			title
		}
		relativePath
	}
	projectPages {
		frontmatter {
			title
			description
		}
		relativePath
	}
}'
headerlink: true
---
# Projects:
I do a lot of projects, some for university, some for ResinDrakeNet, some just as personal projects. They range from mechatronics projects to software projects to gameservers.
Here's some of my projects so far.  
![Engineer Derg](/static/img/engineerderg.png)
