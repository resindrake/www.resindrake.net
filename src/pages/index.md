---
layout: with-image
title: Home
description: ResinDrakeNet - Home
image: {path: /static/img/tallarra_icon.png, link: tallarra_icon}
rainbow: true
query: '{
    pages {
        frontmatter {
            title
        }
        relativePath
    }
}'
---

# Welcome to ResinDrakeNet!
This is mostly a personal website for info on my personal projects, my servers and my refsheet.  

This site is built with <a href="/redirect/stasis">Stasis</a>, a static site generator written in TypeScript; the source for this site can be found <a href="/redirect/source">here</a>.

## Gameservers
All my servers are public, so feel free to check out our servers list and join in on any of our gaming sessions! You can also join my Discord group to hang out with us, receive updates on our servers and join in on voice chats.

Currently my servers are running on a desktop PC repurposed as a serverbox in my living room, running 24/7. I tend to turn quieter servers off when they're not being used. If you want me to turn a server on, don't hesitate to ask!

## Donations
You can support me on [Patreon](/redirect/patreon) and receive some perks for helping out! Donations received will go towards building the server, whether it be new parts, remote hosting, or even small things such as the domain.  
