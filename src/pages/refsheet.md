---
layout: refsheet
title: Refsheet
description: Refsheets and info on my sona
displayRight: true
query: '{
    pages {
        frontmatter {
            title
        }
        relativePath
    }
}'
headerlink: true
---

## Other bits:
* [Maw](/static/refsheet/resin_maw.png) - because neither refsheet really shows my tongue or teeth
* [Alternate eyes](/static/refsheet/resin_eyes.png) - just in case I decide to use these someday
* **Don't colour-pick off the refsheet!**  
Instead, use whatever colour you would naturally use when drawing latex.  
If Resin is wearing latex in the image, the colour should be almost exactly the same.
* **Recent change:** Resin should always have a collar, like in this image:  
<img src="/static/img/tallarra_icon.png" />
