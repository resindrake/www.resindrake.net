---
layout: base
title: Nextcloud
description: Cloud storage server
query: '{
    pages {
        frontmatter {
            title
        }
        relativePath
    }
}'
---

# Nextcloud Server
I host a Nextcloud server at [nextcloud.resindrake.net](/redirect/nextcloud), which I use to store things like ~/Documents, ~/Pictures, etc etc etc, so that they're synced across all my devices and accessible remotely. The exact setup I wanted wasn't possible with Dropbox, so I decided to host it myself.

If for whatever reason you'd like an account on my Nextcloud server, feel free to ask!
