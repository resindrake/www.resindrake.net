---
layout: base
title: Discord Server
description: A public Discord server for ResinDrakeNet
query: '{
    pages {
        frontmatter {
            title
        }
        relativePath
    }
}'
---

# ResinDrakeNet Discord
The ResinDrakeNet Discord server is a server dedicated to gaming with my friends; it's where we organise playthroughs and where we communicate in voice. It's mainly for those interested in gaming, or just hanging out with me and my friends.

The Discord server is public, so [click here](/redirect/discord) to join!
