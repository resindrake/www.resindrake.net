---
layout: base
title: Catapult
description: ENGG100 Catapult
query: '{
    pages {
        frontmatter {
            title
        }
        relativePath
    }
}'
---

# ENGG100 Catapult
The ENGG100 class I did required us to build a catapult capable of launching ping-pong balls for a group project. While the project was intended to be mechanical, I asked *very* nicely, and my group was allowed to use electronics
in our catapult.  
We were required to use the mechanism of a mouse trap as the launching mechanism for our ping-pong balls, but the rest was allowed to be done however we pleased. We had to be able to set the angle of the catapult, as well as fire
with a release mechanism.  

[The source code used for the Arduino can be found
here!](https://gitlab.com/resindrake/catapult-code)  

The angle of the catapult was set electronically using a servo. The servo is controlled by an Arduino Nano, which receives input from a remote control. It was bound between 0 and 90 degrees as going further than this would tangle
wires and be impractical anyway.  

The button on the catapult contracts the solenoid which allows the catapult to fire. Due to technical difficulties and lack of time, we could not trip this mechanism with the remote control, and had to add a bunch of extra
batteries to significantly increase the voltage received by the solenoid.  

Out catapult was successful, able to launch a ping-pong ball quite some distance. The distance varied depending on the angle set by the servo, as you'd expect.  

![Catapult Rotating](/static/projects/catapult/rotate.gif)
![Catapult Firing](/static/projects/catapult/fire.gif)
![atapult Being Launched](/static/projects/catapult/launch.gif)
