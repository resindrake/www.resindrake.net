---
layout: base
title: Synapse
description: resindrake.net Matrix server
query: '{
    pages {
        frontmatter {
            title
        }
        relativePath
    }
}'
---

# Synapse Server
I set up a Synapse server at [matrix.resindrake.net](https://matrix.resindrake.net/_matrix/static/). Currently I'm playing around with bridging Telegram and Discord with it.

For those not familiar with what Matrix is, Matrix is a decentralised chat platform; i.e. there is no central server; just lots of small servers, such that even if a server fails, people can continue chatting.

If you want to try an account on my Matrix server, don't hesitate to ask!

You can chat to me at [@resin:resindrake.net](/redirect/matrix)
