---
layout: base
title: This Site
description: Content for www.resindrake.net
query: '{
    pages {
        frontmatter {
            title
        }
        relativePath
    }
}'
---

# This Site
This site is a fork of [f.0x52.eu](https://f.0x52.eu/); here's [his source](https://git.lain.haus/f0x/site) and [my source](/redirect/source).

I finally got around to working on my own website again! The last one was fairly awful, and was a mess of PHP I barely understood myself. This new website uses [Stasis](https://github.com/Gioni06/stasis-generator) for static site generation, and uses no frontend JS!

## Special Thanks
Special thanks to [YoshiRulz](https://yoshirulz.dev) who gave me a lot of time and help with getting the CSS to work the way I wanted it to.
