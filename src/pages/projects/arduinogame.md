---
layout: base
title: Arduino Sidescroller
description: Sidescrolling Arduino game for ELEC260 at uni
query: '{
    pages {
        frontmatter {
            title
        }
        relativePath
    }
}'
---

# Arduino Sidescroller Game
[Source](https://gitlab.com/resindrake/arduinogame)

An arduino sidescroller where you control a drone/ship that flies through some sort of obstacle course. The drone is armed with a cannon and a detonation charge; the cannon can destroy obstacles in front of the drone and the detonation charge can eliminate all obstacles within a large radius.

You have three lives, and your score is based on your distance travelled and the blocks you hit with the cannon. The highscore is saved to EEPROM so it persists between boots of the Arduino.

Remind me to add pictures later.
