---
layout: side-gallery
title: Towny Minecraft Server
description: ResinDrakeNet public Minecraft server using Towny
query: '{
    pages {
        frontmatter {
            title
        }
        relativePath
    }
}'
gallery: [
    {
        path: /static/projects/townyminecraft/town.jpg,
        link: town,
        display: Kawamachi Village,
        next: {link: house, display: House}
    },
    {
        path: /static/projects/townyminecraft/house.jpg,
        link: house,
        display: Resin's House,
        prev: {link: town, display: Town},
        next: {link: balcony, display: Balcony}
    },
    {
        path: /static/projects/townyminecraft/balcony.jpg,
        link: balcony,
        display: Resin's Balcony,
        prev: {link: house, display: House}
    }
]
---

# Towny Minecraft Server

We started a vanilla Minecraft server at [mc.resindrake.net](/redirect?p=minecraft)! The current theme for the server is Towny, with work being done on our own custom economy plugin. Players live together in towns, and must engage in trading to flourish.  
The server is still a work in progress - jobs are only implemented but only to a rudimentary degree, and will be completely changed later - but the server is open to join! If you're interested in playing, come join us on [Discord](redirect/discord) for more info!
