---
layout: with-image
title: 404
description: There's no page here!
image: {path: /static/img/404.png, link: img}
rainbow: true
query: '{
    pages {
        frontmatter {
            title
        }
        relativePath
    }
}'
---
# 404
That page doesn't exist!
